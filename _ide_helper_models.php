<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Game
 *
 * @property int $id
 * @property mixed $data
 * @property \Illuminate\Support\Carbon $holding_time
 * @property int|null $result
 * @property int $status
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game canceled()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game deferment()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game draw()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game guestWinner()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game hostWinner()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereHoldingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @property int $league_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereLeagueId($value)
 * @property string $host_name
 * @property string $guest_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereGuestName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereHostName($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GamePrediction[] $predictions
 * @property string $league
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereLeague($value)
 * @property string $prediction
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game wherePrediction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game free()
 * @property string $group_id
 * @property-read \App\Group $group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereGroupId($value)
 */
	class Game extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string|null $email_verified_at
 * @property string $password
 * @property int $status
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property bool $is_admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsAdmin($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Group
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Game[] $game
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Group extends \Eloquent {}
}

