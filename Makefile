
COMPOSE_FILES=docker-compose.yml
USER=app
GROUP=app

$(eval CURRENT_UID=$(shell id -u))
$(eval CURRENT_GID=$(shell id -g))


define modify_uid_gid
    $(eval CURRENT_UID=$(shell id -u))
    $(eval CURRENT_GID=$(shell id -g))

    @if [ "$(CURRENT_UID)" -lt "1000" ]; then\
        echo 'You must run target as user has UID >= 1000';\
        exit 1;\
    fi

    @docker-compose -f $(COMPOSE_FILES) exec php sh -c 'usermod $(USER) -u $(CURRENT_UID) && groupmod $(GROUP) -og $(CURRENT_GID)'
    @docker-compose -f $(COMPOSE_FILES) exec php sh -c 'chmod -R 0777 /home/app/laravel/storage'
    @docker-compose -f $(COMPOSE_FILES) exec php sh -c 'usermod -a -G root $(USER);'
    @docker-compose -f $(COMPOSE_FILES) exec php sh -c 'php artisan migrate:refresh'
    @docker-compose -f $(COMPOSE_FILES) exec php sh -c 'php artisan ide-helper:generate'
    @docker-compose -f $(COMPOSE_FILES) exec php sh -c 'php artisan ide-helper:models'
    @docker-compose -f $(COMPOSE_FILES) exec php sh -c 'php artisan ide-helper:meta'
endef

up:
	docker-compose -f $(COMPOSE_FILES) up -d


destroy:
	docker-compose -f $(COMPOSE_FILES) down

status:
	docker-compose -f $(COMPOSE_FILES) ps


shell:
	docker-compose -f $(COMPOSE_FILES) exec --user=$(USER) php zsh

shell-as-root:
	docker-compose -f $(COMPOSE_FILES) exec php sh

provision:
	@$(call modify_uid_gid)
