<?php

use Faker\Generator as Faker;
use App\Game;
use App\Gamer;
use App\League;
use Carbon\Carbon;

$factory->define(Game::class, function (Faker $faker) {
    return [
        Game::HOST_NAME => 'Team ' . rand(1, 100),
        Game::GUEST_NAME => 'Team ' . rand(101, 200),
        Game::LEAGUE => 'League' . rand(0, 1999),
        Game::PREDICTION => 'prediction' . rand(0, 1999),
        Game::HOLDING_TIME => Carbon::now()->subHour(rand(0, 5000)),
        Game::GROUP_ID => factory(\App\Group::class)->create(),
        Game::RESULT => 'test',
        Game::STATUS => 0,
        Game::TYPE => 0
    ];
});
