<?php

use Faker\Generator as Faker;
use App\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        User::NAME => $faker->name,
        User::EMAIL => $faker->unique()->safeEmail,
        User::EMAIL_VERIFIED_AT => now(),
        User::PASSWORD => bcrypt('t12345'),
        User::REMEMBER_TOKEN => str_random(10),
        User::STATUS => array_rand([User::STATUS_ACTIVE, User::STATUS_INACTIVE], 1)
    ];
});
