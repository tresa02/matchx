<?php

use App\Group;
use Faker\Generator as Faker;

$factory->define(App\Group::class, function (Faker $faker) {
    return [
        Group::NAME => $faker->unique()->name(),
        Group::SLUG => $faker->unique()->slug()
    ];
});
