<?php

use App\Group;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Group::_TABLE, function (Blueprint $table) {
            $table->increments(Group::ID);
            $table->string(Group::NAME);
            $table->string(Group::SLUG)->unique()->index();
            $table->timestamps();
        });

        if (env('APP_ENV', 'local') == 'local') {
           factory(Group::class, 10)->create();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
