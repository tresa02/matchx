<?php

use App\Group;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Game;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Game::_TABLE, function (Blueprint $table) {
            $table->increments(Game::ID);
            $table->string(Game::GROUP_ID);
            $table->string(Game::LEAGUE)->index();
            $table->string(Game::PREDICTION)->index();
            $table->string(Game::HOST_NAME)->index();
            $table->string(Game::GUEST_NAME)->index();
            $table->timestamp(Game::HOLDING_TIME);
            $table->string(Game::RESULT)->nullable();
            $table->tinyInteger(Game::STATUS)->default(0);
            $table->tinyInteger(Game::TYPE)->default(0);
            $table->timestamps();

//            $table->foreign(Game::GROUP_ID)->references(Group::_TABLE)->on(Group::ID);// todo::
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Game::_TABLE);
    }
}
