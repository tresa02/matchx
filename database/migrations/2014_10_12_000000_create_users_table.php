<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(User::_TABLE, function (Blueprint $table) {
            $table->increments(User::ID);
            $table->boolean(User::IS_ADMIN)->default(false);
            $table->string(User::NAME);
            $table->string(User::EMAIL)->unique()->index();
            $table->timestamp(User::EMAIL_VERIFIED_AT)->nullable();
            $table->string(User::PASSWORD);
            $table->tinyInteger(User::STATUS)->default(User::STATUS_ACTIVE);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(User::_TABLE);
    }
}
