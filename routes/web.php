<?php

Route::get('/', 'SiteController@index')->name('site.index');
Route::get('groups', 'SiteController@groups')->name('site.groups');

Auth::routes();

Route::get('/home', 'DashboardController@today')->name('home');

Route::middleware('auth')->group(function () {
    Route::get('profile', 'User\ProfileController@index')->name('profile');
    Route::post('profile', 'User\ProfileController@update')->name('profile.update');
    Route::post('profile/pass', 'User\ProfileController@updatePassword')->name('profile.update.password');

    Route::get('game/today', 'DashboardController@today')->name('game.today');
    Route::get('game/tomorrow', 'DashboardController@tomorrow')->name('game.tomorrow');
    Route::get('game/past', 'DashboardController@past')->name('game.past');

});

Route::middleware('auth', 'is_admin')->group(function () {
    Route::resource('user', 'UserController', ['only' => ['index', 'update']]);
    Route::resource('game', 'GameController');
    Route::resource('group', 'GroupController');
});

