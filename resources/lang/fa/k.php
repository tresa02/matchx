<?php
/**
 * keywords
 */
return [
    'game' => 'بازی',
    'name' => 'نام',
    'email' => 'رایانامه',
    'league' => 'لیگ',
    'host' => 'میزبان',
    'guest' => 'میهمان',
    'prediction' => 'پیش بینی',
    'type' => 'نوع',
    'result' => 'نتیجه',
    'holding_time' => 'تاریخ',
    'match' => 'مسابقه',
    'today' => 'امروز',
    'tomorrow' => 'فردا',
    'past' => 'گذشته',
    'management' => 'مدیریت',
    'list' => 'فهرست',
    'add' => 'افزودن',
    'save' => ' ذخیره',
    'reset' => 'ازنو',
    'tell' => 'شماره تماس',
    'slug' => 'نامک',
    'admin' => 'مدیر',
    'user' => 'کاربر',
];