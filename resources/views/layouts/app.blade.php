<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="_csrf" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ url('') }}/assets/images/favicon_1.ico">

    <title>Match X </title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/morris/morris.css">

    <link href="{{ url('') }}/assets/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('') }}/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('') }}/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('') }}/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('') }}/assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('') }}/assets/css/responsive.css" rel="stylesheet" type="text/css" />
    @yield('css')
<!-- jQuery  -->
    <script src="{{ url('') }}/assets/js/jquery.min.js"></script>
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="{{ url('') }}/assets/js/modernizr.min.js"></script>
</head>
<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">
    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="{{ url('/') }}" class="logo"><i class="icon-magnet icon-c-logo"></i>{{ config('production.name') }}</a>
            </div>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left waves-effect waves-light">
                            <i class="md md-menu"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>

                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li class="dropdown top-menu-item-xs">
                            <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                <img src="{{ url('') }}/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle">
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('profile') }}"><i class="ti-user m-r-10 text-custom"></i> پروفایل</a></li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" >
                                        <i class="ti-power-off m-r-10 text-danger"></i> خروج
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    @include('pieces.menu')
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                @include('pieces.errors')
                @yield('content')
            </div> <!-- container -->
        </div> <!-- content -->
        <footer class="footer text-right">
            ©1398. تمام حقوق محفوظ است
        </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>


<script src="{{ url('') }}/assets/js/bootstrap-rtl.min.js"></script>
<script src="{{ url('') }}/assets/js/detect.js"></script>
<script src="{{ url('') }}/assets/js/fastclick.js"></script>
<script src="{{ url('') }}/assets/js/jquery.slimscroll.js"></script>
<script src="{{ url('') }}/assets/js/jquery.blockUI.js"></script>
<script src="{{ url('') }}/assets/js/waves.js"></script>
<script src="{{ url('') }}/assets/js/wow.min.js"></script>
<script src="{{ url('') }}/assets/js/jquery.nicescroll.js"></script>
<script src="{{ url('') }}/assets/js/jquery.scrollTo.min.js"></script>
<!-- jQuery  -->
<script src="{{ url('') }}/assets/plugins/moment/moment.js"></script>
<script src="{{ url('') }}/assets/plugins/notify/notify.js"></script>
<script src="{{ url('') }}/assets/plugins/notify/notify-metro.js"></script>
{{--<script src="{{ url('') }}/assets/plugins/raphael/raphael-min.js"></script>--}}
{{--<script src="{{ url('') }}/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>--}}
<!-- Todojs  -->
{{--<script src="{{ url('') }}/assets/pages/jquery.todo.js"></script>--}}
<!-- chatjs  -->
{{--<script src="{{ url('') }}/assets/plugins/peity/jquery.peity.min.js"></script>--}}
<script src="{{ url('') }}/assets/js/jquery.core.js"></script>
<script src="{{ url('') }}/assets/js/jquery.app.js"></script>
{{--<script src="{{ url('') }}/assets/pages/jquery.dashboard_2.js"></script>--}}

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_csrf"]').attr('content')
        }
    });

    function deleteRecord(el, url){
        var message = "Are you sure?";
        if (confirm(message)) {
            $.post(url,{
                _method:'DELETE',
            }, function (res) {
                el.remove();
                $.Notification.notify('custom','top right','اطلاعیه', res.message)
            });
        }
    }
</script>
@yield('js')
</body>
</html>
