@extends('site.master')
@section('content')

    <!-- Services Section Start -->
    <section id="today_game" class="section-padding">
        <div class="container">
            <div class="section-header text-center">
                <h2 class="section-title wow fadeInDown" >Today's Match Predictions</h2>
                <p>
                    This is a list of free today's forecasts from various leagues
                    make a selection from the list below
                </p>
                <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
            </div>
            <div class="row">
                <!-- Services item -->
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="" data-wow-delay="0.3s">
                        <div class="row">
                            <?php /** @var \App\Game $game */ ?>
                            @foreach($groups as $group)
                               <div class="col-lg-4">
                                   <div class="card card-box ">
                                       <div class="card-header card">
                                           {{ $group->name }}
                                       </div>
                                        <div class="card-body">
                                            Games : <span class="label label-info">{{ $group->game_count }}</span>
                                        </div>
                                   </div>
                               </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Section End -->
@endsection