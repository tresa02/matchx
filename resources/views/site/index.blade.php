@extends('site.master')
@section('menu')
    <li class="nav-item active">
        <a class="nav-link" href="#hero-area">
            Home
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#today_game">
            Today Game
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#services_area">
            Team
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#past_game">
            Last Game
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#contact">
            Contact
        </a>
    </li>
@endsection
@section('hero-aria')
    <div id="hero-area" class="hero-area-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                    <div class="contents">
                        <h2 class="head-title">App, Business & SaaS<br>Landing Page Template</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem repellendus quasi fuga nesciunt dolorum nulla magnam veniam sapiente, fugiat! fuga nesciunt dolorum nulla magnam veniam sapiente, fugiat!</p>
                        <div class="header-button">
                            <a href="{{ route('login') }}" class="btn btn-common">Login</a>
                            <a href="{{ route('register') }}" class="btn btn-border video-popup">Register</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                    <div class="intro-img">
                        <img class="img-fluid" src="{{ url('/fusion') }}/assets/img/intro-mobile.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Services Section Start -->
    <section id="today_game" class="section-padding">
        <div class="container">
            <div class="section-header text-center">
                <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Today's Match Predictions</h2>
                <p>
                    This is a list of free today's forecasts from various leagues
                    make a selection from the list below
                </p>
                <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
            </div>
            <div class="row">
                <!-- Services item -->
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="services-item  fadeInRight" data-wow-delay="0.3s">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>League</th>
                                <th>Match</th>
                                <th>Prediction</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php /** @var App\Game $game */ ?>
                            @foreach($todayGames as $key => $game)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $game->league }}</td>
                                    <td>{{ $game->host_name }} <b style="color: #ff2e50"> VS </b> {{$game->guest_name}}</td>
                                    <td>{{ $game->prediction }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a class="btn btn-primary" href="{{ route('site.groups') }}">More ...</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- About Section start -->
    <div class="about-area section-padding bg-gray" id="services_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-xs-12 info">
                    <div class="about-wrapper wow fadeInLeft" data-wow-delay="0.3s">
                        <div>
                            <div class="site-heading">
                                <p class="mb-3">Manage Statistics</p>
                                <h2 class="section-title">Detailed Statistics of your Company</h2>
                            </div>
                            <div class="content">
                                <p>
                                    Praesent imperdiet, tellus et euismod euismod, risus lorem euismod erat, at finibus neque odio quis metus. Donec vulputate arcu quam. Morbi quis tincidunt ligula. Sed rutrum tincidunt pretium. Mauris auctor, purus a pulvinar fermentum, odio dui vehicula lorem, nec pharetra justo risus quis mi. Ut ac ex sagittis, viverra nisl vel, rhoncus odio.
                                </p>
                                <a href="#" class="btn btn-common mt-3">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
                    <img class="img-fluid" src="{{ url('/fusion') }}/assets/img/about/img-1.png" alt="" >
                </div>
            </div>
        </div>
    </div>
    <!-- About Section End -->

    <!-- Features Section Start -->
    <section id="past_game" class="section-padding">
        <div class="container">
            <div class="section-header text-center">
                <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">
                    Latest Winning Soccer Predictions
                </h2>
                <p>
                    This is a list of recent winnings obtained from our daily soccer prediction. We do our best to ensure you win.
                </p>
                <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="content-left">
                        <div class="box-item wow fadeInLeft" data-wow-delay="0.3s">
                            <table class="table table-striped" style="overflow-x: auto">
                                <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>League</th>
                                    <th>Match</th>
                                    <th>Prediction</th>
                                    <th>Scores</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php /** @var App\Game $game */ ?>
                                @foreach($pastGames as $key => $game)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $game->league }}</td>
                                        <td>{{ $game->host_name }} <b style="color: #ff2e50"> VS </b> {{$game->guest_name}}</td>
                                        <td>{{ $game->prediction }}</td>
                                        <td>{{ $game->result }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Features Section End -->
    <hr>

    <!-- Call To Action Section Start -->
    <section id="cta" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">
                    <div class="cta-text">
                        <h4>Get 30 days free trial</h4>
                        <p>Praesent imperdiet, tellus et euismod euismod, risus lorem euismod erat, at finibus neque odio quis metus. Donec vulputate arcu quam. </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
                    </br><a href="#" class="btn btn-common">Register Now</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Call To Action Section Start -->

    <!-- Contact Section Start -->
    <section id="contact" class="section-padding bg-gray">
        <div class="container">
            <div class="section-header text-center">
                <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Countact Us</h2>
                <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
            </div>
            <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.3s">
                <div class="col-lg-7 col-md-12 col-sm-12">
                    <div class="contact-block">
                        <form id="contactForm">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" required data-error="Please enter your name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Email" id="email" class="form-control" name="email" required data-error="Please enter your email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" placeholder="Subject" id="msg_subject" class="form-control" required data-error="Please enter your subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" id="message" placeholder="Your Message" rows="7" data-error="Write your message" required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="submit-button text-left">
                                        <button class="btn btn-common" id="form-submit" type="submit">Send Message</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-xs-12">
                    <div class="map">
                        <object style="border:0; height: 280px; width: 100%;" data="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d34015.943594576835!2d-106.43242624069771!3d31.677719472407432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86e75d90e99d597b%3A0x6cd3eb9a9fcd23f1!2sCourtyard+by+Marriott+Ciudad+Juarez!5e0!3m2!1sen!2sbd!4v1533791187584"></object>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section End -->

@endsection