<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ config('production.name') }}</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/fusion') }}/assets/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" href="{{ url('/fusion') }}/assets/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{ url('/fusion') }}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ url('/fusion') }}/assets/css/owl.theme.css">

    <!-- Animate -->
    <link rel="stylesheet" href="{{ url('/fusion') }}/assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="{{ url('/fusion') }}/assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="{{ url('/fusion') }}/assets/css/responsive.css">

</head>
<body>

<!-- Header Area wrapper Starts -->
<header id="header-wrap">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <a href="{{ route('site.index') }}" class="navbar-brand"><img src="{{ url('/fusion') }}/assets/img/logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="lni-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
                    @yield('menu')
                    <li class="nav-item">
                        <a class="btn btn-border video-popup" href="{{ route('login') }}">
                            Login
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->

    <!-- Hero Area Start -->
    @yield('hero-aria')
    <!-- Hero Area End -->

</header>
<!-- Header Area wrapper End -->

    @yield('content')

<!-- Footer Section Start -->
<footer id="footer" class="footer-area section-padding">
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <div class="widget">
                        <h3 class="footer-logo"><img src="{{ url('/fusion') }}/assets/img/logo.png" alt=""></h3>
                        <div class="textwidget">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis.</p>
                        </div>
                        <div class="social-icon">
                            <a class="facebook" href="#"><i class="lni-facebook-filled"></i></a>
                            <a class="twitter" href="#"><i class="lni-twitter-filled"></i></a>
                            <a class="instagram" href="#"><i class="lni-instagram-filled"></i></a>
                            <a class="linkedin" href="#"><i class="lni-linkedin-filled"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <h3 class="footer-titel">Products</h3>
                    <ul class="footer-link">
                        <li><a href="#">Tracking</a></li>
                        <li><a href="#">Application</a></li>
                        <li><a href="#">Resource Planning</a></li>
                        <li><a href="#">Enterprise</a></li>
                        <li><a href="#">Employee Management</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <h3 class="footer-titel">Resources</h3>
                    <ul class="footer-link">
                        <li><a href="#">Payment Options</a></li>
                        <li><a href="#">Fee Schedule</a></li>
                        <li><a href="#">Getting Started</a></li>
                        <li><a href="#">Identity Verification</a></li>
                        <li><a href="#">Card Verification</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <h3 class="footer-titel">Contact</h3>
                    <ul class="address">
                        <li>
                            <a href="#"><i class="lni-map-marker"></i> 105 Madison Avenue - <br> Third Floor New York, NY 10016</a>
                        </li>
                        <li>
                            <a href="#"><i class="lni-phone-handset"></i> P: +84 846 250 592</a>
                        </li>
                        <li>
                            <a href="#"><i class="lni-envelope"></i> E: contact@uideck.com</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright-content">
                        <p>Copyright © 2020 <a rel="nofollow" href="https://uideck.com">UIdeck</a> All Right Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->

<!-- Go to Top Link -->
<a href="#" class="back-to-top">
    <i class="lni-arrow-up"></i>
</a>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ url('/fusion') }}/assets/js/jquery-min.js"></script>
<script src="{{ url('/fusion') }}/assets/js/popper.min.js"></script>
<script src="{{ url('/fusion') }}/assets/js/bootstrap.min.js"></script>
<script src="{{ url('/fusion') }}/assets/js/owl.carousel.min.js"></script>
<script src="{{ url('/fusion') }}/assets/js/wow.js"></script>
<script src="{{ url('/fusion') }}/assets/js/jquery.nav.js"></script>
<script src="{{ url('/fusion') }}/assets/js/scrolling-nav.js"></script>
<script src="{{ url('/fusion') }}/assets/js/jquery.easing.min.js"></script>
<script src="{{ url('/fusion') }}/assets/js/main.js"></script>
<script src="{{ url('/fusion') }}/assets/js/form-validator.min.js"></script>
<script src="{{ url('/fusion') }}/assets/js/contact-form-script.min.js"></script>

</body>
</html>
