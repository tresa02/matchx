@extends('auth.master')
@section('content')
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center">
                ایجاد حساب کاربری
                <strong>
                    <a href="{{ url('') }}" class="text-custom">{{ config('production.name') }}</a>
                </strong>
            </h3>
        </div>
        @include('pieces.errors')
        <div class="panel-body">
            <form class="form-horizontal m-t-20" action="{{ route('register') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="name" type="text" placeholder="نام" >
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="email" type="email" placeholder="ایمیل">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" name="password" type="password" required="" placeholder="پسورد">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" name="password_confirmation" type="password" required="" placeholder="تکرار پسورد">
                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
                            عضویت
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">
            <p>
                حساب کاربری دارید؟<a href="{{ route('login') }}" class="text-primary m-l-5"><b>وارد شوید</b></a>
            </p>
        </div>
    </div>

</div>
@endsection