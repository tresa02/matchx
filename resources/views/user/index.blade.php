@extends('layouts.app')
<?php /** @var \App\User $users */ ?>
@section('css')
    <link type="text/css" rel="stylesheet" href="{{ url('') }}/assets/plugins/datatable/jquery.dataTables.css" />
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped" id="datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('k.name') }}</th>
                        <th>{{ __('k.email') }}</th>
                        <th>{{ __('k.type') }}</th>
                        {{--<th>Status</th>--}}
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $key => $user)
                    <tr data-id="{{ $user->id }}">
                        <td>{{ ++$key }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if($user->is_admin)
                                <span class="user_admin btn btn-xs btn-sm btn-primary">{{ __('k.admin') }}</span>
                            @else
                                <span class="user_admin btn btn-xs btn-sm btn-warning">{{ __('k.user') }}</span>
                            @endif
                        </td>
                        {{--<td>--}}
                            {{--@if($user->status)--}}
                                {{--<span class="user_status btn btn-xs btn-sm btn-success">Active</span>--}}
                            {{--@else--}}
                                {{--<span class="user_status btn btn-xs btn-sm btn-warning">Inactive</span>--}}
                            {{--@endif--}}
                        {{--</td>--}}
                        <td>
                            -
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ url('') }}/assets/plugins/datatable/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $('#datatable').dataTable({
            "pageLength": 50
        });
    </script>

    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function() {--}}
            {{--$('span.user_status').click(function () {--}}
                {{--var _id = $(this).parent().parent().attr('data-id');--}}
                {{--if (confirm('Are You Sure ?')) {--}}
                    {{--$.post(--}}
                        {{--'{{ route('user.index') }}/' + _id,--}}
                        {{--{--}}
                            {{--type: 'status',--}}
                            {{--_method: 'PUT'--}}
                        {{--},--}}
                        {{--function (res) {--}}
                            {{--alert(res.message);--}}
                        {{--}--}}
                    {{--);--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}

    <script type="text/javascript">
        $(document).ready(function() {
            $('span.user_admin').click(function () {
                var el = $(this);
                var _id = $(this).parent().parent().attr('data-id');
                if (confirm('Are You Sure ?')) {
                    $.post(
                        '{{ route('user.index') }}/' + _id,
                        {
                            type: 'admin',
                            _method: 'PUT'
                        },
                        function (res) {
                           if(res.user.is_admin) {
                               el.html("{{ __('k.admin') }}");
                               el.removeClass('btn-warning');
                               el.addClass('btn-primary');
                           } else {
                               el.html("{{ __('k.user') }}");
                               el.removeClass('btn-primary');
                               el.addClass('btn-warning');
                           }
                            $.Notification.notify('custom','top left','اطلاعیه', res.message)
                        }
                    );
                }
            });
        });
    </script>
@endsection
