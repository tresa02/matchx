<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">اصلی</li>
                <li class="has_sub">
                    <a href="{{ route('home') }}" class="waves-effect"><i class="ti-home"></i> <span> داشبورد </span> </a>
                </li>
                <li  class="has_sub">
                    <a href="{{ route('game.today') }}" class="waves-effect"><i class="ti-cloud-down"></i> <span> بازی های امروز</span> </a>
                    <a href="{{ route('game.tomorrow') }}" class="waves-effect"><i class="ti-cloud-up"></i> <span> بازی های فردا</span> </a>
                    <a href="{{ route('game.past') }}" class="waves-effect"><i class="ti-cloud-up"></i> <span> بازی های قبل</span> </a>
                </li>
                @if(auth()->user()->is_admin)
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="ti-paint-bucket"></i>
                        <span> مدیریت بازی ها  </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('group.index') }}">گروه های پیش بینی</a></li>
                        <li><a href="{{ route('game.create') }}">افزودن بازی</a></li>
                        <li><a href="{{ route('game.index') }}">نمایش کل بازی ها</a></li>
                    </ul>
                </li>
                <li >
                    <a href="{{ route('user.index') }}" class="waves-effect"><i class="ti-user"></i> <span> کاربران</span> </a>
                </li>
                @endif
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
