@extends('layouts.app')
<?php /** @var \App\User $users */ ?>
@section('css')
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <h3>گروه های پیش بینی</h3>
                <div id="jsGrid"></div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
    <script type="text/javascript">
        var clients = {!! $groups !!};
    </script>
    <script>
        $("#jsGrid").jsGrid({
            width: "100%",
            filtering:true,
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,

            data: clients,
            onItemInserting: function(args) {
                var url = '{{ route('group.store') }}';
                $.post(
                    url,
                    {
                        name:args.item.name,
                        slug:args.item.slug,
                    },
                    function (res) {
                        $.Notification.notify('custom','top left', '{{ __('k.message') }}', res.message)
                    }
                );
            },
            onItemUpdated: function(args) {
                var url = '{{ route('group.index') }}/' + args.item.id;
                $.post(
                    url,
                    {
                        _method:'PATCH',
                        name:args.item.name,
                        slug:args.item.slug,
                    },
                    function (res) {
                        $.Notification.notify('custom','top left', '{{ __('k.message') }}', res.message)
                    }
                );
            },
            onItemDeleted: function(args) {
                var url = '{{ route('group.index') }}/' + args.item.id;
                $.post(
                    url,
                    {
                        _method:'DELETE',
                    },
                    function (res) {
                        $.Notification.notify('custom','top left', '{{ __('k.message') }}', res.message)
                    }
                );
            },
            fields: [
                { name: "name", type: "text", validate: "required" },
                { name: "slug", type: "text", validate: "required" },
                { type: "control" }
            ]
        });
    </script>
@endsection
