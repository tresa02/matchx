@extends('layouts.app')

@section('content')
   @include('game.game_table', $games)
    {{--modal--}}
    @include('game.edit_modal')
@endsection
