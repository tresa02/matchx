<div id="game-edit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="pg-add-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form form-inline" action="" id="game-form-edit" method="post">
                    <input type="hidden" value="PUT" name="_method">
                    @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger waves-effect waves-light btn-facebook dropdown-toggle"  aria-expanded="false">میزبان</button>
                            </div>
                            <input name="host" type="text" id="g-host-edit" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger waves-effect waves-light btn-facebook dropdown-toggle"  aria-expanded="false">لیگ</button>
                            </div>
                            <input name="league" type="text" id="g-league-edit" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger waves-effect waves-light btn-facebook dropdown-toggle"  aria-expanded="false">گروه</button>
                            </div>
                            <select name="group" type="text" id="g-group-edit" class="form-control" >
                                @foreach($groups as $group)
                                    <option value="{{ $group->id }}"> {{ $group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary waves-effect waves-light btn-facebook dropdown-toggle"  aria-expanded="false">میهمان</button>
                            </div>
                            <input name="guest" type="text" id="g-guest-edit" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-facebook dropdown-toggle"  aria-expanded="false"> روز مسابقه</button>
                            </div>
                            <input type="date"  id="m-holding-edit-date" class="form-control" name="holding_date">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group" id='datetimepicker3'>
                            <div class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-facebook dropdown-toggle"  aria-expanded="false"> ساعت مسابقه</button>
                            </div>
                            <input type="text" id="m-holding-edit-time" class="form-control timepickers" name="holding_time">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-facebook dropdown-toggle"  aria-expanded="false"> پییش بینی</button>
                            </div>
                            <input type="text" id="g-prediction-edit" class="form-control " name="prediction">
                        </div>
                    </div>

                    <hr>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> {{__('k.save')}}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('.timepickers').datetimepicker({
            format: 'HH:mm'
        });
    });
    $('button#edit').click(function (e) {
        var _id = $(this).attr('data-id');
        $.get(
            '{{ route('game.index') }}/' + _id + '/edit',
            {},
            function(res){
                if (res.find == true) {
                    showEditModal(res)
                }
            }
        );
    });

    function showEditModal(game) {
        $('#g-group-edit').val(game.group_id);
        $('#g-guest-edit').val(game.guest_name);
        $('#g-host-edit').val(game.host_name);
        $('#g-league-edit').val(game.league);
        $('#g-prediction-edit').val(game.prediction);
        $('#m-holding-edit-time').val(game.holding_time);
        $('#m-holding-edit-date').val(game.holding_date);
        $('#game-form-edit').attr('action', '{{ route('game.index') }}/' + game.id);
        $('#game-edit-modal').modal();
    }

</script>