<div class="row">
    <div class="col-md-12">
        <div class="">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <h4 class="m-t-0 header-title"><b>مسابقات</b></h4>
                        <p class="text-muted font-13 m-b-30"></p>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ __('k.league') }}</th>
                                <th>{{ __('k.host') }}</th>
                                <th>{{ __('k.guest') }}</th>
                                <th>{{ __('k.holding_time') }}</th>
                                <th>{{ __('k.prediction') }}</th>
                                <th>{{ __('k.result') }}</th>
                                <th>{{ __('k.type') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php /** @var \Illuminate\Support\Collection $games  */ ?>
                            <?php /** @var \App\Game $game  */ ?>
                            @foreach($games as $key => $game)
                            <tr>
                                <td> {{ ++$key }}</td>
                                <td>{{ $game->league }}</td>
                                <td>{{ $game->host_name }}</td>
                                <td>{{ $game->guest_name }}</td>
                                <td>{{ $game->holding_time }}</td>
                                <td>{{ $game->prediction }}</td>
                                <td>{{ $game->result }}</td>
                                <td>{{ __('gameType.' . $game->type) }}</td>
                                <td>
                                    <button class="btn btn-sm btn-primary" id="edit" data-id="{{$game->id}}">ویرایش <i class="fa fa-edit"></i> </button>
                                    <button class="btn btn-sm btn-danger" id="delete"
                                            onclick="deleteRecord(
                                                   $(this).parent().parent(),
                                                       '{{ route('game.destroy', $game->id) }}'
                                                       )">حذف  <i class="fa fa-remove"> </i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $games->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>