@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/datatable/jquery.dataTables.css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="card-box">
                    <div class="row">
                        <h3><i class="fa fa-list"></i> لیست پیش بینی ها</h3>
                        <hr>
                        <table id="datatable" class="table table-striped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>{{ __('k.league') }}</td>
                                <td>{{ __('k.match') }}</td>
                                <td>{{ __('k.holding_time') }}</td>
                                <td>{{ __('k.prediction') }}</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $game)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $game['league'] }}</td>
                                <td>{{ $game['host'] }}
                                    <strong style="color: #b91d19;">VS</strong>
                                    {{ $game['guest'] }}</td>
                                <td>{{ $game['holding_time'] }}</td>
                                <td>{{ $game['prediction'] }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('') }}/assets/plugins/datatable/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $('#datatable').dataTable({
            "pageLength": 20
        });
    </script>
@endsection
