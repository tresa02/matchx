@extends('layouts.app')

@section('css')
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />
@endsection
@section('content')
    <?php /** @var \App\Game $game */ ?>
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="card-box">
                    <div class="row">
                      <div class="col-lg-8 col-lg-offset-2">
                          <h3 style="text-align: center"> host : <span style="color: #628cff"> {{ $game->host_name }}</span>
                              - guest :  <span style="color: #ff4a7b">  {{ $game->guest_name }}</span>
                          </h3>
                          <p style="text-align: center"> holding time : {{ $game->holding_time }}</p>
                          <p style="text-align: center"> لیگ : {{ $game->league }}</p>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div id="jsGrid"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
    <script type="text/javascript">
        var data = {!! $game->predictions->toJson() !!};
    </script>

    <script type="text/javascript">
        var items = [
            { Name: "نامشخص", Id: 0 },
            { Name: "درست", Id: 1 },
            { Name: "غلط", Id: 2 },
        ];

        $("#jsGrid").jsGrid({
            width: "100%",
            height: "400px",

            inserting: true,
            editing: true,
            sorting: true,
            paging: true,

            data: data,

            onItemInserting: function(args) {
                $.post(
                    '{{ route('prediction.store', $game->id) }}',
                    {
                        prediction: args.item.prediction,
                        description: args.item.description,
                        status: args.item.status
                    },
                    function(res) {

                    }
                );
            },
            onItemUpdating: function(args) {
                $.post(
                    '{{ route('prediction.store', $game->id) }}/' + args.item.id,
                    {
                        _method: 'put',
                        prediction: args.item.prediction,
                        description: args.item.description,
                        status: args.item.status
                    },
                    function(res) {

                    }
                );
            },
            onItemDeleting: function(args) {
                $.post(
                    '{{ route('prediction.store', $game->id) }}/' + args.item.id ,
                    {
                      _method:'delete'
                    },
                    function(res) {
                        console.log(res);
                    }
                );
            },
            fields: [
                { name: "prediction", title: 'پیش بینی', type: "text", width: 150, validate: "required" },
                { name: "description", title: 'توضیحات', type: "text", width: 200 },
                { name: "status", title: 'وضعیت', type: "select", items: items, valueField: "Id", textField: "Name" ,validate: "required"},
                { type: "control" }
            ]
        });
    </script>
@endsection
