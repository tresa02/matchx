@extends('layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"
          rel="stylesheet"/>
@endsection
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>افزودن بازی</b></h4>
                <p class="p-b-10"></p>
                <form action="{{ route('game.store') }}" class="form" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">لیگ</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="league">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">میزبان</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="host">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">میهمان</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="guest">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">پیش بینی</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="prediction">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">تاریخ</label>
                                <div class="col-md-10">
                                    <input type="date" class="form-control" name="holding_date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"> ساعت</label>
                                <div class="col-md-10">
                                    <input type="text" id="g-holding-edit-time" class="form-control timepicker"
                                           name="holding_time">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">نوع</label>
                                <div class="col-md-10">
                                    <select type="text" class="form-control" name="type">
                                        <option value="0">{{ __('gameType.0') }}</option>
                                        <option value="1">{{ __('gameType.1') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">گروه</label>
                                <div class="col-md-10">
                                    <select type="text" class="form-control" name="group">
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <button class="btn btn-sm btn-primary" type="submit">
                        <i class="fa fa-check"></i> {{__('k.save')}}
                    </button>
                    <button class="btn btn-sm btn-inverse" type="reset">
                        <i class="fa fa-refresh"></i> {{__('k.reset')}}
                    </button>
                </form>
            </div>
        </div>
    </div>
    {{--table--}}
    @include('game.game_table', $games)

    {{--modal--}}
    @include('game.edit_modal')

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $('.timepicker').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>
@endsection
