@extends('layouts.app')

@section('content')
    <div class="row">
        {{--profile--}}
        <div class="col-sm-6">
            <div class="card-box">
                <h4 class="m-t-0
header-title"><b>اطلاعات پروفایل</b></h4>

                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" role="form" method="post" action="{{ route('profile.update') }}">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">نام</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="name" value="{{ auth()->user()->name }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="email">ایمیل</label>
                                <div class="col-md-10">
                                    <input type="email" value="{{ auth()->user()->email }}" id="email" name="email" class="form-control"
                                           placeholder="ایمیل">
                                </div>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-md btn-primary"  type="submit">ذخیره </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{--change password--}}

        <div class="col-sm-6">
            <div class="card-box">
                <h4 class="m-t-0 header-title">
                    <b>تغیر کلمه عبور</b>
                </h4>

                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" role="form" method="post" action="{{ route('profile.update') }}">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">پسورد قبلی</label>
                                <div class="col-md-10">
                                    <input type="password" class="form-control"  name="old_password" placeholder="پسورد قبلی">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> پسورد جدید</label>
                                <div class="col-md-10">
                                    <input type="password" class="form-control" name="password" placeholder=" پسورد جدید">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="password_confirmation">تکرار پسورد</label>
                                <div class="col-md-10">
                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control"
                                           placeholder="تکرار پسورد">
                                </div>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-md btn-primary"  type="submit">ذخیره </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection