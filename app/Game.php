<?php

namespace App;

use App\ModelScopes\GameScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Game
 *
 * @property int $id
 * @property mixed $data
 * @property \Illuminate\Support\Carbon $holding_time
 * @property int|null $result
 * @property int $status
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game canceled()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game deferment()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game draw()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game guestWinner()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game hostWinner()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereHoldingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @property int $league_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereLeagueId($value)
 * @property string $host_name
 * @property string $guest_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereGuestName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereHostName($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GamePrediction[] $predictions
 * @property string $league
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereLeague($value)
 * @property string $prediction
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game wherePrediction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game free()
 * @property string $group_id
 * @property-read \App\Group $group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereGroupId($value)
 */
class Game extends Model
{
    const _TABLE = 'games';

    const ID = 'id';
    const GROUP_ID = 'group_id';
    const LEAGUE = 'league';
    const HOST_NAME = 'host_name';
    const GUEST_NAME = 'guest_name';
    const HOLDING_TIME = 'holding_time';
    const PREDICTION = 'prediction';
    const RESULT = 'result';
    const STATUS = 'status';
    const TYPE = 'type';
    const TYPE_FREE = '0';
    const TYPE_VIP = '1';

    protected $table = self::_TABLE;
    protected $guarded = [];
    protected $dates = [self::HOLDING_TIME];


    /**
     * retrieve group
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(
            Group::class,
            self::GROUP_ID,
            Group::ID
        );
    }

    /**
     * get today game
     *
     * @param $query
     *
     * @return mixed
     */
    public function ScopeTodayGame($query)
    {
        return $query->where(self::HOLDING_TIME, '>=', Carbon::today())
            ->where(self::HOLDING_TIME, '<', Carbon::tomorrow());
    }

    /**
     * get tomorrow game
     *
     * @param $query
     *
     * @return mixed
     */
    public function ScopeTomorrowGame($query)
    {
        return $query->where(self::HOLDING_TIME, '>=', Carbon::tomorrow())
            ->where(self::HOLDING_TIME, '<', Carbon::tomorrow()->addDay(1));
    }

    /**
     * get past game
     *
     * @param $query
     *
     * @return mixed
     */
    public function ScopePastGame($query)
    {
        return $query->where(self::HOLDING_TIME, '<', Carbon::today());
    }

    /**
     * get free game
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeFree($query)
    {
        return $query->where(self::TYPE, self::TYPE_FREE);
    }
}
