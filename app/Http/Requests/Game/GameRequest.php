<?php

namespace App\Http\Requests\Game;

use Illuminate\Foundation\Http\FormRequest;

class GameRequest extends FormRequest
{
    const GROUP = 'group';
    const LEAGUE = 'league';
    const PREDICTION = 'prediction';
    const HOST = 'host';
    const GUEST = 'guest';
    const HOLDING_TIME = 'holding_time';
    const HOLDING_DATE = 'holding_date';
    const TYPE = 'type';
    const STATUS = 'status';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::GROUP => 'required',
            self::LEAGUE => 'required',
            self::PREDICTION => 'required',
            self::HOST => 'required',
            self::GUEST => 'required',
            self::HOLDING_TIME => 'required',
            self::HOLDING_DATE => 'required'
        ];
    }
}
