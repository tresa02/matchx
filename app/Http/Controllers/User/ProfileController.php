<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Controllers\Controller;
use Hash;

class ProfileController extends Controller
{
    /**
     * show profile form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('profile');
    }

    /**
     * update user data
     *
     * @param \App\Http\Requests\UpdateProfileRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProfileRequest $request)
    {
        $user = auth()->user();
        $user->name = $request->get('name', ' ');
        $user->email = $request->get('email', ' ');
        $user->phone = $request->get('phone', ' ');

        if ($user->isDirty()) {
            $user->save();
            $message = 'change user detail is success';
        } else {
            $message = 'no change ';
        }

        return back()->with('message', $message);
    }

    /**
     * change user password
     *
     * @param \App\Http\Requests\ChangePasswordRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        if (Hash::check($request->get('old_password'), auth()->user()->password)) {
            /** @var \App\User $user */
            $user = auth()->user();
            $user->password = bcrypt($request->get('password'));
            if ($user->isDirty()) {
                $user->save();
                $message = 'change password successful';
            } else {
                $message = 'no change.';
            }
        } else {
            $message = "old password is wrong";
        }

        return back()->with('message', $message);
    }
}
