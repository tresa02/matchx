<?php

namespace App\Http\Controllers;

use App\Game;
use App\Group;
use App\Http\Requests\Game\GameRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::orderBy(Game::HOLDING_TIME, 'desc')->paginate(25);
        $groups = Group::all();

        return view('game.index', compact('games', 'groups'));
    }

    /**
     * Show the resource.
     *
     * @param \App\Game $game
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Game $game)
    {
        return view('game.show', compact('game'));
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $games = Game::orderBy(Game::ID, 'desc')->take(100)->paginate(25);
        $groups = Group::all();

        return view('game.create', compact('games', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Game\GameRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(GameRequest $request)
    {
        $game = new Game();
        $this->setGameValues($request, $game);
        $game->save();

        return back()->with('message', 'game created successfully');
    }

    /**
     * Display the specified resource to edit.
     *
     * @param \App\Game $game
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Game $game)
    {
        $data = $game->toArray();
        $data[Game::HOLDING_TIME] = $game->holding_time->format('H:i');
        $data['holding_date'] = $game->holding_time->format('Y-m-d');
        $data['find'] = true;

       return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Game\GameRequest $request
     * @param \App\Game                           $game
     *
     * @return \Illuminate\Http\Response
     */
    public function update(GameRequest $request, Game $game)
    {
        $this->setGameValues($request, $game);
        $game->save();

        if ($request->isXmlHttpRequest()) {
            return response()->json(['message' => 'updated', 'finish' => true]);
        } else {
            return back()->with('message', 'updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Game $game
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Game $game)
    {
       $game->delete();

       return response()->json(['message' => 'game Deleted']);
    }

    /**
     * set game values from request
     *
     * @param \App\Http\Requests\Game\GameRequest $request
     * @param                                     $game
     */
    private function setGameValues(GameRequest $request, $game): void
    {
        $game->group_id = $request->get(GameRequest::GROUP);
        $game->league = $request->get(GameRequest::LEAGUE);
        $game->host_name = $request->get(GameRequest::HOST);
        $game->guest_name = $request->get(GameRequest::GUEST);
        $game->prediction = $request->get(GameRequest::PREDICTION);
        $game->holding_time = Carbon::parse(
            $request->get(GameRequest::HOLDING_DATE) . ' ' . $request->get(GameRequest::HOLDING_TIME)
        )->format('Y-m-d H:i:s');
    }
}
