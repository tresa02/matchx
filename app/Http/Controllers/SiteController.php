<?php

namespace App\Http\Controllers;

use App\Game;
use App\Group;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * site index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $todayGames = Game::todayGame()->take(8)->get();
        $pastGames = Game::pastGame()->take(8)->get();

        return view('site.index', compact('todayGames', 'pastGames'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function groups()
    {
        $groups = Group::has('game', '>', '0')->withCount('game')->take(21)->get();

        return view('site.groups', compact('groups'));
    }
}
