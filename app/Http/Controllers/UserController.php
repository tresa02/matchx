<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', '!=', auth()->user()->id)->get();

        return view('user.index', compact('users'));
    }

    public function update(Request $request, User $user)
    {
        if ($request->get('type') == 'status') {
            $user->status = !$user->status;
        } elseif ($request->get('type') == 'admin') {
            $user->is_admin = !$user->is_admin;
        }
        $user->save();

        return response()->json([
           'message' => 'user updated.',
           'finish' => true,
           'user' => $user
        ]);
    }
}
