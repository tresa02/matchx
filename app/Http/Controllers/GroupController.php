<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\GroupRequest;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::orderBy(Group::ID, 'desc')->get();

        return view('group.index', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\GroupRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {
        $group = new Group();
        $group->name = $request->get(GroupRequest::NAME);
        $group->slug = $request->get(GroupRequest::SLUG);
        $group->save();

        return response()->json(
            [
                'message' => 'Group created.',
                'error' => false
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $group->name = $request->get(GroupRequest::NAME);
        $group->slug = $request->get(GroupRequest::SLUG);
        $group->save();

        return response()->json(
            [
                'message' => 'Group updated.',
                'error' => false
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Group $group)
    {
        $group->delete();

        return response()->json(
            [
                'message' => 'Group deleted.',
                'error' => false
            ]
        );
    }
}
