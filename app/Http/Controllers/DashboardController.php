<?php

namespace App\Http\Controllers;

use App\Game;
use App\GamePrediction;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * show today game page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function today()
    {
        $games = Game::todayGame()->free()->get();
        $data = $this->reserveGameData($games);

        return view('game.table', compact('data'));
    }

    /**
     * show today game page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tomorrow()
    {
        $games = Game::tomorrowGame()->free()->get();
        $data = $this->reserveGameData($games);

        return view('game.table', compact('data'));
    }

    /**
     * show today game page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function past()
    {
        $games = Game::pastGame()->free()->get();
        $data = $this->reserveGameData($games);

        return view('game.table', compact('data'));
    }

    /**
     * @param $games
     * @return \Illuminate\Support\Collection
     */
    public function reserveGameData($games): \Illuminate\Support\Collection
    {
        $data = collect();
        /** @var Game $game */
        foreach ($games as $game) {
            $data->push(
                [
                    'id' => $game->id,
                    'host' => $game->league,
                    'guest' => $game->guest_name,
                    'league' => $game->host_name,
                    'holding_time' => $game->holding_time,
                    'prediction' => $game->prediction,
                ]
            );
        }
        return $data;
    }
}
